<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class GdprController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'clean';
        return $this->render('gdpr');
    }

}
