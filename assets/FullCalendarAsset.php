<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Fullcalendar asset bundle.
 */
class FullCalendarAsset extends AssetBundle
{
    public $sourcePath = '@bower/fullcalendar';
    public $css = [
        '/dist/fullcalendar.css'
    ];
    public $js = [
        '/dist/fullcalendar.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\MomentAsset'
    ];
}
