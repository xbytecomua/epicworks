<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Fullcallendar Scheduler asset bundle.
 *
 */
class FullCalendarSchedulerAsset extends AssetBundle
{
    public $sourcePath = '@bower/fullcalendar-scheduler';
    public $css = [
        '/dist/scheduler.css'
    ];
    public $js = [
        '/dist/scheduler.js'
    ];
    public $depends = [
        'app\assets\FullCalendarAsset'
    ];
}
