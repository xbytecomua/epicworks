<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Momentjs asset bundle.
 *
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@bower/moment';
    public $css = [
    ];
    public $js = [
        '/min/moment.min.js'
    ];
    public $depends = [
    ];
}
