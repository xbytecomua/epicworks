<?php

namespace app\common\components;
use yii\base\BootstrapInterface;
class LanguageSelector implements BootstrapInterface
{

    public $supportedLanguages = [];

    public function bootstrap($app)
    {
        if(php_sapi_name() === 'cli')
        {
            return true;
        }

        $languageNew = \Yii::$app->request->get('language');
        if($languageNew)
        {
            if(in_array($languageNew, $this->supportedLanguages))
            {
                \Yii::$app->language = $languageNew;
                $cookies = \Yii::$app->response->cookies;
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'language',
                    'value' => $languageNew
                ]));
            }
        }
        elseif( \Yii::$app->getRequest()->getCookies()->has( 'language' ) )
        {
            \Yii::$app->language = \Yii::$app->getRequest()->getCookies()->getValue( 'language' );
        }
        else
        {
            $preferredLanguage = $app->request->getPreferredLanguage($this->supportedLanguages);
            \Yii::$app->language=$preferredLanguage;
        }

    }
}
