<?php

namespace app\modules\calendar\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `calendar` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        //echo Url::toRoute();
        echo Yii::$app->urlManager->createUrl(['/events', 'start' => 105,'end'=>106]);
    }
    public function actionEvents($start,$end)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data  =  $end;
    }
}
