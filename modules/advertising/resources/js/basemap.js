var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});
var OpenStreetMap_DE = L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});
var OpenPtMap = L.tileLayer('http://openptmap.org/tiles/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openptmap.org">OpenPtMap</a> contributors'
});
var Opnv = L.tileLayer('http://tile.memomaps.de/tilegen/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.öpnvkarte.de">öpnvkarte</a> contributors'
});

var Thunderforest_Transport = L.tileLayer('https://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=3bb263151d0d46c09f2e532ff7aabcb4', {
    attribution: '&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    apikey: '<your apikey>',
    maxZoom: 22
});
var OpenMapSurfer_Roads = L.tileLayer('https://maps.heigit.org/openmapsurfer/tiles/roads/webmercator/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});

var bingAeral = L.tileLayer.bing({bingMapsKey : "AuhiCJHlGzhg93IqUH_oCpl_-ZUrIE6SPftlyGYUvr9Amx5nzA-WqGcPquyFZl4L",imagerySet : "AerialWithLabels", culture : "de"});
var bingRoad = L.tileLayer.bing({bingMapsKey : "AuhiCJHlGzhg93IqUH_oCpl_-ZUrIE6SPftlyGYUvr9Amx5nzA-WqGcPquyFZl4L",imagerySet : "Road", culture : "de"});
var googleSatellite = L.gridLayer.googleMutant({type: 'hybrid', maxZoom :18});// valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
var googleRoad = L.gridLayer.googleMutant({type: 'roadmap', maxZoom :18});// valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'

var map = L.map('map', {
    layers: [OpenStreetMap_DE],
    contextmenu: true,
    contextmenuWidth: 200,
    contextmenuItems: [
        {
            text: translateStrings.showCoordinates,
            callback: showCoordinates
        },
        {
            text: translateStrings.centerMap,
            callback: centerMap
        },
        '-',
        {
            text: translateStrings.zoomIn,//translate
            icon: contextMenuPath + '/examples/images/zoom-in.png',
            callback: zoomIn
        },
        {
            text: translateStrings.zoomOut,//translate
            icon: contextMenuPath + '/examples/images/zoom-out.png',
            callback: zoomOut
        }
    ]
});
//////////////////////////////////////////
function showCoordinates (e) {
    alert(e.latlng);
}
function centerMap (e) {
    map.panTo(e.latlng);
}
function zoomIn () {
    map.zoomIn();
}
function zoomOut () {
    map.zoomOut();
}
/////////////////////////////////////////
if (!map.restoreView()) {
    map.setView([0, 0], 0);
}

var baseMaps = {
    "OSM DE": OpenStreetMap_DE,
    "Mapnik": OpenStreetMap_Mapnik,
    "OPNVkarte":Opnv,
    "BingRoad" : bingRoad,
    "BingAeral": bingAeral,
    "GoogleRoad" : googleRoad,
    "GoogleSatellite" : googleSatellite,
    "Thunderforest.Transport": Thunderforest_Transport,
    "OpenMapSurfer.Roads": OpenMapSurfer_Roads
};

var overlayMaps = {
    "Transport": OpenPtMap
};

L.control.layers(baseMaps, overlayMaps).addTo(map);

var ZoomViewer = L.Control.extend({
    options: {
        position: 'bottomleft'
    },
    onAdd: function(){

        var container= L.DomUtil.create('div');
        var gauge = L.DomUtil.create('div');
        container.style.width = '200px';
        container.style.background = 'rgba(255,255,255,0.5)';
        container.style.textAlign = 'left';
        gauge.innerHTML = 'Zoom level: ' + map.getZoom();
        map.on('zoomstart zoom zoomend', function(){
            gauge.innerHTML = 'Zoom level: ' + map.getZoom();
        });
        container.appendChild(gauge);

        return container;
    }
});

(new ZoomViewer).addTo(map);

document.addEventListener("keydown", function(e){
    if (e.shiftKey || e.which == 16){
        map.options.zoomSnap = 0.1;
        map.options.zoomDelta = .0336;
    }
});
document.addEventListener("keyup", function(e){
    if (e.shiftKey || e.which == 16){
        map.options.zoomSnap = 1;
        map.options.zoomDelta = 1;
    }
});

let trackControl = L.Control.fileLayerLoad({
    // Allows you to use a customized version of L.geoJson.
    // For example if you are using the Proj4Leaflet leaflet plugin,
    // you can pass L.Proj.geoJson and load the files into the
    // L.Proj.GeoJson instead of the L.geoJson.
    layer: L.geoJson,
    // See http://leafletjs.com/reference.html#geojson-options
    layerOptions: {style: {color:'red'}},
    // Add to map after loading (default: true) ?
    addToMap: true,
    // File size limit in kb (default: 1024) ?
    fileSizeLimit: 5120,
    // Restrict accepted file formats (default: .geojson, .json, .kml, and .gpx) ?
    formats: [
        '.geojson',
        '.kml',
        '.gpx'
    ]
}).addTo(map);

trackControl.loader.on('data:loaded', function (event) {
    console.log(event);
});

