let layerGroup = new L.FeatureGroup().addTo(map);

let regionGeojson = L.geoJSON(null,{
    style: {
        "color": "#34495e",
        "fillOpacity": 0.1,
        "opacity": 1,
        "weight": 5
    },
    onEachFeature: function(feature, layer){
        layerGroup.addLayer(layer);
        if(layer.feature.properties.title && layer.feature.properties.center){
            let marker = createPlzMarker(layer.feature.properties.center, layer.feature.properties.title);
            marker.addTo(map);
            layer.marker = marker;
        }

    }
});

function createPlzMarker(coordinates, text){

    let icon = createPlzMarkerIcon(text);
    return L.marker(new L.LatLng(coordinates[1], coordinates[0]),{icon: icon, draggable: true});
}

function createPlzMarkerIcon(text){
    return L.divIcon({
        className: 'text-marker label small',
        html: text, iconSize: [60, 25], iconAnchor: [25, 10]
    });
}

if(geoJson){
    regionGeojson.addData(geoJson);
}

map.fitBounds(regionGeojson.getBounds());

let printControl = L.Control.extend({
    options: {
        title: 'Print page',
        position: 'topleft',
        sizeModes: ['A4Portrait', 'A4Landscape'],
        defaultMode:'A4Landscape',
        defaultSizeTitles: {
            A4Landscape: 'A4 Landscape',
            A4Portrait: 'A4 Portrait',
        }
    },

    onAdd: function (map) {
        this.options.sizeModes = this.options.sizeModes.map(function (sizeMode) {
            if (sizeMode === 'A4Landscape') {
                return {
                    height: this._a4PageSize.height,
                    width: this._a4PageSize.width,
                    name: this.options.defaultSizeTitles.A4Landscape,
                    className: 'A4Landscape page',

                }
            }
            if (sizeMode === 'A4Portrait') {
                return {
                    height: this._a4PageSize.width,
                    width: this._a4PageSize.height,
                    name: this.options.defaultSizeTitles.A4Portrait,
                    className: 'A4Portrait page'
                }
            }
            return sizeMode;
        }, this);

        this._map = map;
        this.mapContainer = this._map.getContainer();
        this.wrapper = L.DomUtil.getSizedParentNode(this.mapContainer);
        this.table = L.DomUtil.get('table');
        this.css = document.createElement("style");
        this.css.type = "text/css";
        this.css.innerHTML = `@media print {
          img { max-width: 98%!important; max-height: 98%!important; }
          @page { size: landscape ;}}
        `;
        document.body.appendChild(this.css);

        this._resize(this.options.defaultMode);



        //controls
        //print button
        let container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');

        this.link = L.DomUtil.create('a', 'leaflet-control-print-button', container);
        this.link.id = "leafletPrint";
        this.link.title = this.options.title;

        L.DomEvent.addListener(this.link, 'click', this._print, this);
        //format button
        this.holder = L.DomUtil.create('ul', 'printHolder', container);
        this.options.sizeModes.forEach(function (sizeMode) {
            let btn = L.DomUtil.create('li', 'printSizeMode', this.holder);
            btn.title = sizeMode.name;
            L.DomUtil.create('a', sizeMode.className, btn);
            L.DomEvent.addListener(btn, 'click', this._resize, this);
        }, this);

        return container;
    },

    _resize: function(event){
        var sizeMode = typeof event !== 'string' ? event.target.className : event;
        var pageSize = this.options.sizeModes.filter(function (item) {
            return item.className.indexOf(sizeMode) > -1;
        });
        pageSize = pageSize[0];
        this.wrapper.style.width = pageSize.width + 'px';
        this.wrapper.style.height = (pageSize.height-this.table.scrollHeight) + 'px';
        if (this.wrapper.style.width > this.wrapper.style.height) {
            this._changeOrientation('portrait');
        } else {
            this._changeOrientation('landscape');
        }
        setTimeout(function(){ map.invalidateSize()}, 200);
        setTimeout(function(){ map.fitBounds(regionGeojson.getBounds())}, 400);


    },
    _print: function(){
      window.print();
    },

    _changeOrientation: function(orientation){
        this.css.innerHTML = `@media print {
          img { max-width: 98%!important; max-height: 98%!important; }
          @page { size: ` + orientation + `;}}
        `;
    },

    _a4PageSize: {
        height: 746,
        width: 1066
    }
});

map.addControl(new printControl());