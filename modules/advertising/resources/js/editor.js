"use strict";
(function (window, document, undefined) {
    //bind contextmenu to layer
    L.Layer.prototype.addMenu = function(){
        this.options.bubblingMouseEvents = false;
        this.bindContextMenu({
            contextmenu: true,
            contextmenuInheritItems: true,
            contextmenuItems: [
                '-',
                {
                    text: translateStrings.changeRegionConfiguration,
                    callback: function (e) {
                        let layer = e.relatedTarget;
                        enableObjectDataEditor(layer);
                    }
                },
                {
                    text: translateStrings.print,
                    callback: function (e) {
                        let layer = e.relatedTarget;
                        printObject(layer);
                    }
                }
            ]
        })
    };

    //add interactive(clickable) option to a layer for a feature posibility to toogle interactive state
    L.Layer.prototype.setInteractive = function (interactive) {
        if (this.getLayers) {
            this.eachLayer(function(layer){
                layer.setInteractive(interactive);
            });
            return;
        }

        if (!this._path) {
            return;
        }

        this.options.interactive = interactive;

        if (interactive) {
            L.DomUtil.addClass(this._path, 'leaflet-interactive');
        } else {
            L.DomUtil.removeClass(this._path, 'leaflet-interactive');
        }
    };
    ////////////////////////////////////////////////////////////////
    //add method to layergroup to search leayer by id
    L.LayerGroup.include({
        customGetLayer: function (id) {
            for (let i in this._layers) {
                if (this._layers[i].id === id) {
                    return this._layers[i];
                }
            }
        }
    });
    //unbind contextmenu from all layers in layergroup
    L.LayerGroup.include({
        disableContextmenu: function(){
            map.contextmenu.disable();
            this.invoke('unbindContextMenu');
        }
    });
    //bind contextmenu for all layers in layergroup
    L.LayerGroup.include({
        enableContextmenu: function(){
            map.contextmenu.enable();
            this.invoke('addMenu');
        }
    });
    //////////////////////////////////////////
    //*Extend Leaflet Draw Classes//////////////////////
    L.EditToolbar.include({
        getActions: function (handler) {
            let actions = [
                {
                    title: L.drawLocal.edit.toolbar.actions.save.title,
                    text: L.drawLocal.edit.toolbar.actions.save.text,
                    callback: this._save,
                    context: this
                },
                {
                    title: L.drawLocal.edit.toolbar.actions.cancel.title,
                    text: L.drawLocal.edit.toolbar.actions.cancel.text,
                    callback: this.disable,
                    context: this
                }
            ];
            return actions;
        }
    });

    L.EditToolbar.Delete.include({
        removeAllLayers: function () {
            // Iterate of the delateable layers and add remove them
            this._deletableLayers.eachLayer(function (layer) {
                this._removeLayer({layer: layer});
            }, this);
        },
        enable: function () {
            if (this._enabled || !this._hasAvailableLayers()) {
                return;
            }

            this.fire('enabled', {handler: this.type});
            this._map.fire(L.Draw.Event.DELETESTART, {handler: this.type});
            L.Handler.prototype.enable.call(this);

            this._deletableLayers
                .on('layeradd', this._enableLayerDelete, this)
                .on('layerremove', this._disableLayerDelete, this);

            this.removeAllLayers();
        },
        addHooks: function () {
            let map = this._map;

            if (map) {
                map.getContainer().focus();
                this._deletableLayers.eachLayer(this._enableLayerDelete, this);
                this._deletedLayers = new L.LayerGroup();
            }
        },
        // @method removeHooks(): void
        // Remove listener hooks from this handler
        removeHooks: function () {
            if (this._map) {
                this._deletableLayers.eachLayer(this._disableLayerDelete, this);
                this._deletedLayers = null;
            }
        },
        _enableLayerDelete: function (e) {
            let layer = e.layer || e.target || e;

            layer.on('click', this._removeLayer, this);
            if(layer.marker instanceof L.Marker)
            {
                this._deletableLayers.addLayer(layer.marker);
            }
        },

    });

    ////////////////////////////////////////////////////
    //* Fix touch supported browsers bug with big icons in leaflet draw *//
    L.Edit.PolyVerticesEdit.addInitHook(function(){
        if (!(L.Browser.mobile && L.Browser.touch)) {
            this.options.icon = new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon'
            });
        }
    });
    L.Draw.Polyline.addInitHook(function(){
        if (!(L.Browser.mobile && L.Browser.touch)) {
            this.options.icon = new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon'
            });
        }
    });
    L.Edit.SimpleShape.addInitHook(function(){
        if (!(L.Browser.mobile && L.Browser.touch)) {
            this.options.moveIcon = new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon leaflet-edit-move'
            });
            this.options.resizeIcon =  new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon leaflet-edit-resize'
            });
        }
    });
    //* End fix *//

    /**
     * Leaflet.REditor assumes that you have already included the Leaflet library.
     */
    /**
     * @class L.REditor
     */
    L.REditor = L.Class.extend({

        options: {
        },

        initialize: function(map, options) {
            this._map = map;
            L.setOptions(this, options);
        }
    });

    L.REditor.Select = L.Handler.extend({

        options:{
            layerGroup: undefined,
            editLayer: undefined
        },
        addHooks: function() {
            this.layerGroup.on('click', this._toggleSelect, this);
            this.layerGroup.on('click', this._makeEditable, this);
            this.layerGroup.on('contextmenu', this._contextSelect, this);
            this.layerGroup.on('contextmenu', this._makeEditable, this);
            this.editLayer.on('click', this._toggleSelect, this);
            this.editLayer.on('click', this._editableRevert, this);
            this.editLayer.enableContextmenu();
            this.layerGroup.enableContextmenu();
        },

        removeHooks: function() {
            this.layerGroup.off('click', this._toggleSelect, this);
            this.layerGroup.off('click', this._makeEditable, this);
            this.layerGroup.off('contextmenu', this._contextSelect, this);
            this.layerGroup.off('contextmenu', this._makeEditable, this);
            this.editLayer.off('click', this._toggleSelect, this);
            this.editLayer.off('click', this._editableRevert, this);
            this.editLayer.disableContextmenu();
            this.layerGroup.disableContextmenu();
        },

        _contextSelect: function(e){
            if(this._selected === e.layer){
                return;
            }
            this._resetHighlight();
            this._setSelected(e.layer);
        },

        _toggleSelect:function(e){
            this._resetHighlight();

            //unselect if already selected
            if(this._selected === e.layer)
            {
                this._selected = false;
                return;
            }

            this._setSelected(e.layer);
        },

        _makeEditable: function (e) {
            this._editableRevert();
            e.layer.removeFrom(e.target);
            e.layer.addTo(this.editLayer);
        },

        _editableRevert: function () {
            this.editLayer.eachLayer(function (layer) {
                layer.removeFrom(this.editLayer);
                layer.addTo(this.layerGroup);
            },this);
        },

        _setSelected:function(layer){
            this._selected = layer;
            this._highlightFeature(layer);
        },

        _highlightFeature: function(layer){

            layer.setStyle({"color": "#FF0000"});

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }
        },

        _resetHighlight: function(){
            if(!this._selected) return;
            this._selected.setStyle(this._selected.defaultOptions.style);
        },

        clearSelections: function () {
            this._resetHighlight();
            this._selected = false;
        },

        initialize: function(map, options) {
            L.Util.setOptions(this, options);
            L.Handler.prototype.initialize.call(this, map);
            this.layerGroup = this.options.layerGroup;
            this.editLayer  = this.options.editLayer;
            this._map.on(L.Draw.Event.EDITSTART,this.disable, this);
            this._map.on(L.Draw.Event.EDITSTOP,this.enable, this);
            this._map.on(L.Draw.Event.DELETESTART,this.disable, this);
            this._map.on(L.Draw.Event.DELETESTOP,this.enable, this);
            this._map.on(L.Draw.Event.DRAWSTART,this.clearSelections, this);
            this._map.on(L.Draw.Event.DELETESTOP,this.enable, this);
        }

    });

})(window, document);
///////////////////////////

let sidebar = $('#control-sidebar'),
form = $('#region-data');

form.inputs = {
    'id' : $('#objectdataform-id'),
    'title' : $('#objectdataform-title'),
    'lon' : $('#objectdataform-longitude'),
    'lat' : $('#objectdataform-latitude')
};

//Disable editable plugin and remove leaflet draw toolbal on sidebar open
sidebar.on('expanded.controlsidebar', function(){
    map.editable.disable();
    map.removeControl(drawControl);
});

//Enable editable plugin add leaflet draw  and clean form on sidebar open
sidebar.on('collapsed.controlsidebar', function() {
    map.editable.enable();
    map.addControl(drawControl);
    disableObjectDataEditor();
});

function resetForm(){
    form.yiiActiveForm('resetForm');
    form[0].reset();
    form.data('yiiActiveForm').validated = false;
}

function enableObjectDataEditor(layer)
{
    if(layer.marker instanceof L.Marker)
    {
        layer.marker.dragging.enable();
        layer.marker.on('dragend', copyMarkerCoordinatesToForm);
        layer.on('click', moveMarker);
    }
    copyObjectDataToForm(layer);
    layer.on('click', copyClickCoordinatesToForm);
    sidebarToggle();
}

function disableObjectDataEditor()
{
    let layer = map.editable._selected;
    if(layer.marker instanceof L.Marker)
    {
        layer.marker.dragging.disable();
        layer.marker.off('dragend', copyMarkerCoordinatesToForm);
        layer.off('click', moveMarker);
    }
    layer.off('click', copyClickCoordinatesToForm);
    resetForm();
}
function printObject(layer) {
    let layerId = layer.feature.properties.id;
    window.open(actions.print+'?id='+layerId, '_blank', 'location=yes,scrollbars=yes,status=yes')
}

function sidebarToggle() {
    sidebar.trigger("click");
}

function moveMarker(e)
{
    let layer = e.target, marker = layer.marker;
    marker.setLatLng(e.latlng);
}

function copyMarkerCoordinatesToForm(e){
    let marker = e.target;
    form.inputs.lon.val(marker.getLatLng().lng);
    form.inputs.lat.val(marker.getLatLng().lat);
}

function copyClickCoordinatesToForm(e) {
    form.inputs.lon.val(e.latlng.lng);
    form.inputs.lat.val(e.latlng.lat);
}

function copyObjectDataToForm(layer) {
    let data = layer.feature.properties;
    form.inputs.id.val(data.id || '');
    form.inputs.title.val(data.title || '');
    if(Array.isArray(data.center))
    {
        form.inputs.lon.val(data.center[0]  || '');
        form.inputs.lat.val(data.center[1]  || '');
    }
}

//////////End Contextmenu functions///////////////
//////////////////////////////////////////////////
let ajaxConfig = {resetForm:true, messageLoading: translateStrings.loading};
yii2AjaxRequest('#region-data', ajaxConfig,
    (success)   =>  {
        let layer = map.editable._selected,
        properties = layer.feature.properties;

        properties.id = success.data.id;
        properties.title = success.data.title;
        properties.center = success.data.center;
        if(layer.marker instanceof L.Marker)
        {
            let icon = createPlzMarkerIcon(success.data.title);
            layer.marker.setIcon(icon);
            let newLatLng = new L.LatLng(properties.center[1], properties.center[0]);
            layer.marker.setLatLng(newLatLng);
        }
        else {
            layer.marker = createPlzMarker(properties.center, success.data.title).addTo(map);
        }
        sidebarToggle();
    },
    (error) =>  {

    });




function createPlzMarkerIcon(text){
    return L.divIcon({
        className: 'text-marker label small',
        html: text, iconSize: [50, 20], iconAnchor: [25, 10]
    });
}
function createPlzMarker(coordinates, text){

    let icon = createPlzMarkerIcon(text);
    return L.marker(new L.LatLng(coordinates[1], coordinates[0]),{icon: icon});
}
//////////////////////////////////////////////////////////////
let editLayer = new L.FeatureGroup().addTo(map);
let layerGroup = new L.FeatureGroup().addTo(map);
let markers = L.markerClusterGroup({
    showCoverageOnHover: false}
).addTo(map);

map.addHandler('editable',function(){
    return new L.REditor.Select(map,
        {
            editLayer: editLayer,
            layerGroup: layerGroup
        }
    );
});

map.editable.enable();

let regionGeojson = L.geoJSON(null,{
    style: {
        "color": "#34495e",
        "fillOpacity": 0.1,
        "opacity": 1,
        "weight": 5
    },
    onEachFeature: function(feature, layer){
        layerGroup.addLayer(layer);
        layer.addMenu();
        if(layer.feature.properties.title && layer.feature.properties.center){
           let marker = createPlzMarker(layer.feature.properties.center, layer.feature.properties.title);
           marker.addTo(markers);
           layer.marker = marker;
        }

    }
});

$.getJSON(actions.list, function(data){
    regionGeojson.addData(data);
});

let drawOptions = {
    position: 'topleft',
    draw: {
        polyline: false,
        rectangle: false,
        circle: false,
        marker: false,
        circlemarker: false,
        polygon: {
            allowIntersection: true,
            showArea: true,
            drawError: {
                color: '#ff0000',
                timeout: 1000
            }
        }
    },
    edit: {
        featureGroup: editLayer,
        remove: true
    }
};

let drawControl = new L.Control.Draw(drawOptions);
map.addControl(drawControl);

map.on(L.Draw.Event.CREATED, function (e) {
    let layer=e.layer,
    geojsonFeature=layer.toGeoJSON(),
    data=JSON.stringify(geojsonFeature);
    $.ajax({
        type: 'POST',
        url: actions.create,
        data: {"ObjectGeometryForm":{"geojson": data}},
        dataType: 'json',
        success: function(responseData, textStatus, jqXHR) {
            geojsonFeature.properties.id = responseData.data.id;
            regionGeojson.addData(geojsonFeature);
        },
        error: function (responseData, textStatus, errorThrown) {
            console.log(textStatus);
        }
    });
});

map.on(L.Draw.Event.EDITED, function(e){
        e.layers.eachLayer(function(layer) {
            let geojsonFeature = layer.toGeoJSON(),
            data = JSON.stringify(geojsonFeature);
            $.ajax({
                type: 'POST',
                url: actions.editGeometry + '/' + geojsonFeature.properties.id,
                data: {"ObjectGeometryForm": {"geojson": data}},
                dataType: 'json',
                success: function (responseData, textStatus, jqXHR) {

                },
                error: function (responseData, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        });
});

map.on(L.Draw.Event.DELETED,function(e){
    e.layers.eachLayer(function(layer) {
        if(layer instanceof L.Marker) return;
        let geojsonFeature = layer.toGeoJSON();
        $.ajax({
            type: 'POST',
            url: actions.delete + '/' + geojsonFeature.properties.id,
            dataType: 'text',
            success: function (responseData, textStatus, jqXHR) {

            },
            error: function (responseData, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    });
});







