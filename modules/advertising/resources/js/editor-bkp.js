(function (window, document, undefined) {
    L.Layer.prototype.setInteractive = function (interactive) {
        if (this.getLayers) {
            this.eachLayer(function(layer){
                layer.setInteractive(interactive);
            });
            return;
        }
        if (!this._path) {
            return;
        }

        this.options.interactive = interactive;

        if (interactive) {
            L.DomUtil.addClass(this._path, 'leaflet-interactive');
        } else {
            L.DomUtil.removeClass(this._path, 'leaflet-interactive');
        }
    };
    ////////////////////////////////////////////////////////////////
    L.LayerGroup.include({
        customGetLayer: function (id) {
            for (var i in this._layers) {
                if (this._layers[i].id == id) {
                    return this._layers[i];
                }
            }
        }
    });
    //////////////////////////////////////////
    //*Extend Leaflet Draw Classes//////////////////////
    L.EditToolbar.include({
        getActions: function (handler) {
            var actions = [
                {
                    title: L.drawLocal.edit.toolbar.actions.save.title,
                    text: L.drawLocal.edit.toolbar.actions.save.text,
                    callback: this._save,
                    context: this
                },
                {
                    title: L.drawLocal.edit.toolbar.actions.cancel.title,
                    text: L.drawLocal.edit.toolbar.actions.cancel.text,
                    callback: this.disable,
                    context: this
                }
            ];
            return actions;
        }
    });

    L.EditToolbar.Delete.include({
        removeAllLayers: function () {
            // Iterate of the delateable layers and add remove them
            this._deletableLayers.eachLayer(function (layer) {
                this._removeLayer({layer: layer});
            }, this);
        },
        enable: function () {
            if (this._enabled || !this._hasAvailableLayers()) {
                return;
            }
            this.fire('enabled', {handler: this.type});

            this._map.fire(L.Draw.Event.DELETESTART, {handler: this.type});

            L.Handler.prototype.enable.call(this);

            this._deletableLayers
                .on('layeradd', this._enableLayerDelete, this)
                .on('layerremove', this._disableLayerDelete, this);

            this.removeAllLayers();
        },
        addHooks: function () {
            var map = this._map;

            if (map) {
                map.getContainer().focus();
                this._deletableLayers.eachLayer(this._enableLayerDelete, this);
                this._deletedLayers = new L.LayerGroup();
            }
        },
        // @method removeHooks(): void
        // Remove listener hooks from this handler
        removeHooks: function () {
            if (this._map) {
                this._deletableLayers.eachLayer(this._disableLayerDelete, this);
                this._deletedLayers = null;
            }
        }
    });
    ////////////////////////////////////////////////////
    //* Fix touch supported browsers bug with big icons *//

    L.Edit.PolyVerticesEdit.addInitHook(function(){
        if (!(L.Browser.mobile && L.Browser.touch)) {
            this.options.icon = new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon'
            });
        }
    });
    L.Draw.Polyline.addInitHook(function(){
        if (!(L.Browser.mobile && L.Browser.touch)) {
            this.options.icon = new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon'
            });
        }
    });
    L.Edit.SimpleShape.addInitHook(function(){
        if (!(L.Browser.mobile && L.Browser.touch)) {
            this.options.moveIcon = new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon leaflet-edit-move'
            });
            this.options.resizeIcon =  new L.DivIcon({
                iconSize: new L.Point(8, 8),
                className: 'leaflet-div-icon leaflet-editing-icon leaflet-edit-resize'
            });
        }
    });
    //* End fix *//

    /**
     * Leaflet.REditor assumes that you have already included the Leaflet library.
     */
    L.reditorVersion = '0.0.1';
    /**
     * @class L.REditor
     */
    L.REditor = L.Class.extend({
        options: {

        },

        initialize: function(map, options) {
            this._map = map;
            L.setOptions(this, options);
        }

    });

    L.REditor.Select = L.Handler.extend({
        options:{
            layerGroup: undefined,
            styles: {
                highlight: {
                    "color": "#FF0000",
                    "fillOpacity": 0.1,
                    "opacity": 1,
                    "weight": 5
                }
            }
        },
        selected: false,
        addHooks: function() {
            this.layerGroup.on('click', this._toggleSelect, this);
        },

        removeHooks: function() {
            this.layerGroup.off('click', this._toggleSelect, this);
        },

        _toggleSelect:function(e){
            console.log(e.layer);//Clean
            this._resetHighlight();
            if(this._selected === e.layer)
            {
                this._selected = false;
                return;
            }
            this._setSelected(e.layer);
        },

        _setSelected:function(layer){
            this._selected = layer;
            this._highlightFeature(layer);
        },

        _highlightFeature: function(layer){
            style = this._getStyle('highlight');
            layer.setStyle(style);

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }
        },

        _getStyle: function(style){
            return this.options.styles[style];
        },

        _resetHighlight: function(){
            if(!this._selected) return;
            this._selected.setStyle(this._selected.defaultOptions.style);
        },

        _createFeaturesLayer: function () {
            return this.options.layerGroup || new L.FeatureGroup().addTo(this._map);
        },

        clearSelections: function () {
            this._resetHighlight();
            this._selected = false;
        },

        getLayerGroup: function(){
            return this.layerGroup;
        },

        initialize: function(map, options) {
            L.Util.setOptions(this, options);
            L.Handler.prototype.initialize.call(this, map);
            this.layerGroup = this._createFeaturesLayer();
        }

    });

    L.REditor.Editable = L.REditor.Select.extend({
        options: {
            editLayer: undefined,
        },

        addHooks: function() {
            L.REditor.Select.prototype.addHooks.call(this);
            this.editLayer.on('click',this._toggleSelect, this);
            this.editLayer.on('click',this._editableRevert, this);
            this.layerGroup.on('click', this._makeEditable, this);
        },

        removeHooks: function() {
            L.REditor.Select.prototype.removeHooks.call(this);
            this.editLayer.off('click',this._toggleHighlight, this);
            this.editLayer.off('click',this._editableRevert, this);
            this.layerGroup.off('click', this._makeEditable, this);
        },

        _createEditLayer: function () {
            return this.options.editLayer || new L.FeatureGroup().addTo(this._map);
        },

        getEditLayer: function(){
            return this.editLayer;
        },

        _makeEditable: function (e) {
            this._editableRevert();
            e.layer.removeFrom(e.target);
            e.layer.addTo(this.editLayer);
        },

        _editableRevert: function () {
            this.editLayer.eachLayer(function (layer) {
                layer.removeFrom(this.editLayer);
                layer.addTo(this.layerGroup);
            },this);
        },

        clearSelections:function () {
            this._editableRevert();
            L.REditor.Select.prototype.clearSelections.call(this);
        },

        initialize: function(map, options) {
            L.Util.setOptions(this, options);
            L.REditor.Select.prototype.initialize.call(this, map);
            this.editLayer = this._createEditLayer();

            ////Move to watcher handler
            this._map.on(L.Draw.Event.EDITSTART,this.disable, this);
            this._map.on(L.Draw.Event.EDITSTOP,this.enable, this);
            this._map.on(L.Draw.Event.DELETESTART,this.disable, this);
            this._map.on(L.Draw.Event.DELETESTOP,this.enable, this);
            this._map.on(L.Draw.Event.DRAWSTART,this.clearSelections, this);
            /////Move to watcher handler
            this.enable();
        }
    });

})(window, document);

var editLayer = new L.FeatureGroup().addTo(map);
var layerGroup = new L.FeatureGroup().addTo(map);
var markerGroup = L.markerClusterGroup({
    showCoverageOnHover: false,
    zoomToBoundsOnClick: true
}).addTo(map);

var regionGeojson = L.geoJSON(null,{
    style: {
        "color": "#34495e",
        "fillOpacity": 0.1,
        "opacity": 1,
        "weight": 5
    },
    onEachFeature: function(feature, layer){
        layer.options.bubblingMouseEvents = false;
        layer.bindContextMenu({
            contextmenu: true,
            contextmenuInheritItems: false,
            contextmenuItems: [{
                text: 'Marker item',
                callback: function (e) {
                    console.log(e);
                }
            }]
        });
    layerGroup.addLayer(layer);
    }
});

$.getJSON(actions.list, function(data){
    regionGeojson.addData(data);
});

map.addHandler('editable',function(){
    return new L.REditor.Select(map,
        {
            editLayer: editLayer,
            layerGroup: layerGroup
        }
    );
});
map.editable.enable();

var drawOptions = {
    position: 'topleft',
    draw: {
        polyline: false,
        rectangle: false,
        circle: false,
        marker: false,
        circlemarker: false,
        polygon: {
            allowIntersection: true,
            showArea: true,
            drawError: {
                color: '#ff0000',
                timeout: 1000
            }
        }
    },
    edit: {
        featureGroup: editLayer,
        remove: true
    }
};
var drawControl = new L.Control.Draw(drawOptions);
map.addControl(drawControl);

map.on(L.Draw.Event.CREATED, function (e) {
    var layer=e.layer,
    geojsonFeature=layer.toGeoJSON(),
    data=JSON.stringify(geojsonFeature);
    $.ajax({
        type: 'POST',
        url: actions.create,
        data: {"GeometryForm":{"geojson": data}},
        dataType: 'text',
        success: function(responseData, textStatus, jqXHR) {
            geojsonFeature.properties.id = responseData;
            regionGeojson.addData(geojsonFeature);
        },
        error: function (responseData, textStatus, errorThrown) {
            console.log(textStatus);
        }
    });
});

map.on(L.Draw.Event.EDITED, function(e){
        e.layers.eachLayer(function(layer) {
            var geojsonFeature = layer.toGeoJSON(),
            data = JSON.stringify(geojsonFeature);
            $.ajax({
                type: 'POST',
                url: actions.editGeometry + '/' + geojsonFeature.properties.id,
                data: {"GeometryForm": {"geojson": data}},
                dataType: 'text',
                success: function (responseData, textStatus, jqXHR) {

                },
                error: function (responseData, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        });
});

map.on(L.Draw.Event.DELETED,function(e){
    e.layers.eachLayer(function(layer) {
        var geojsonFeature = layer.toGeoJSON();
        $.ajax({
            type: 'POST',
            url: actions.delete + '/' + geojsonFeature.properties.id,
            dataType: 'text',
            success: function (responseData, textStatus, jqXHR) {

            },
            error: function (responseData, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    });

});

var test = function(e){
  console.log(e);
};


// var renderFeature = function(feature, layer){
//     // if(feature.properties.object_title && feature.properties.object_center) {
//     //     var icon = L.divIcon({
//     //          className: 'marker plz small',
//     //          html: feature.properties.object_title, iconSize: [60, 18], iconAnchor: [30, 9]
//     //     });
//     //     new L.Marker(feature.properties.object_center,{
//     //         icon: icon
//     //     }).addTo(markerGroup);
//     //     feature.properties.center = center;
//     // }
//
//     layer.bindContextMenu({
//         contextmenu: true,
//         contextmenuItems: [{
//             text: 'Marker item',
//             callback: createMarker
//         }
//         ,
//         {
//             text: 'Edit Info',
//             callback: editInfo,
//         }
//         ]
//     });
//     layerGroup.addLayer(layer);
// };
// var createMarker = function(e){
//     console.log(e);
//     var icon = L.divIcon({
//          className: 'marker plz small',
//          html: e.relatedTarget.feature.properties.object_title ? e.relatedTarget.feature.properties.object_title : '00000-0', iconSize: [60, 18], iconAnchor: [30, 9]
//     });
//     new L.Marker(e.latlng,{
//         icon: icon
//     }).addTo(markerGroup);
//
// };
//
// var editInfo = function (e) {
//     var title,
//         lat,
//         lng;
//     console.log(e)
//     // title = e.relatedTarget.feature.properties.title ? relatedTarget.feature.properties.title : '';
//     // $('#objectinfoform-longitude').val(e.latlng.lng);
//     // $('#objectinfoform-latitude').val(e.latlng.lat);
//     // new L.Marker(e.latlng).addTo(markerGroup);
//     // $('#control-sidebar').click();
// };
map.on('contextmenu.show',function(e){
    console.log('Context menu fired on :');
    console.log(e);
    console.log('-');
});
// map.on('contextmenu',function(){
//    console.log('contextmenu clicked');
// });



