
var objectLayers = new L.FeatureGroup();
objectLayers.addTo(map);

$.getJSON('/data.json', function(data){
    regionGeojson = L.geoJson(data,{
        style:{
            "color": "#c0392b",
            "fillColor":"#c0392b",
            "fillOpacity": 0.1,
            "opacity": 1,
            "weight": 3,

        },
        onEachFeature: function (feature, layer) {
            layer.addTo(objectLayers);
        }
    });
});

objectLayers.on('click',function (e) {
   var layer = e.layer;
   layer.pm.enable();
   console.log(layer);

});
objectLayers.on('contextmenu',function (e) {
    var layer = e.layer;
    layer.pm.disable();
    b = layer.toGeoJSON();
});

// optional options
// define toolbar options
var options = {
    position: 'topleft', // toolbar position, options are 'topleft', 'topright', 'bottomleft', 'bottomright'
    drawMarker: true, // adds button to draw markers
    drawPolyline: true, // adds button to draw a polyline
    drawRectangle: true, // adds button to draw a rectangle
    drawPolygon: {
        templineStyle: {
            color: 'green'
        }
    },
    // adds button to draw a polygon
    drawCircle: true, // adds button to draw a cricle
    cutPolygon: true, // adds button to cut a hole in a polygon
    dragPolygon: false,
    editMode: false, // adds button to toggle edit mode for all layers
    removalMode: false // adds a button to remove layers
    // the lines between coordinates/markers
};
var opt2 = {
  position:'topleft',
    editMode: true, // adds button to toggle edit mode for all layers
    removalMode: true // adds a button to remove layers

};
// add leaflet.pm controls to the map
map.pm.addControls(options);
map.pm.addControls(opt2);

map.on('pm:create', function(e) {
    var layer = e.layer;
    layer.removeFrom(this);
    layer.addTo(objectLayers);
    console.log(layer.toGeoJSON())

});
map.on('pm:edit', function(e) {
    console.log('edit');
    var layer = e.layer;
    layer.removeFrom(this);
    layer.addTo(objectLayers);
    console.log(layer.toGeoJSON())

});
map.on('pm:cut', function(e) {
    console.log(e);
    var layer = e.resultingLayers[0];
    layer.removeFrom(this);
    layer.addTo(objectLayers);
    console.log(layer.toGeoJSON())
});






