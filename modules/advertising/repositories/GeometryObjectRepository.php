<?php

namespace app\modules\advertising\repositories;

use app\modules\advertising\models\GeometryObject;

class GeometryObjectRepository
{
    /**
     * @param $id
     * @return GeometryObject
     * @throws \InvalidArgumentException
     */
    public function find($id)
    {
        if ( !$geoObject = GeometryObject::findOne($id)){
            throw new \InvalidArgumentException('Model not found');
        }
        return $geoObject;
    }

    /**
     * @param $id
     * @return GeometryObject
     * @throws \InvalidArgumentException
     */
    public function load($id)
    {
        if (
            !$geoObject = GeometryObject::find()
            ->select('id, title, ST_AsText(center) as `center`, ST_AsText(geometry) as `geometry`')
            ->andWhere('id= :id', [':id' => $id])
            ->one()
        )

        {
            throw new \InvalidArgumentException('Model not found');
        }

        return $geoObject;
    }

    public function findAll()
    {
        if (!$objectList = GeometryObject::find()->select('id, title, ST_AsText(center) as `center`, ST_AsText(geometry) as `geometry`')->active()->all())
        {
            throw new \InvalidArgumentException('Model not found');
        }
        return $objectList;
    }

    /**
     * @param $geoObject GeometryObject
     * @throws \InvalidArgumentException
     */
    public function add($geoObject)
    {
        if (!$geoObject->getIsNewRecord()) {
            throw new \InvalidArgumentException('Model not exists');
        }
        $geoObject->insert(false);
    }

    /**
     * @param $geoObject GeometryObject
     * @throws \InvalidArgumentException
     */
    public function save($geoObject)
    {
        if ($geoObject->getIsNewRecord()) {
            throw new \InvalidArgumentException('Model not exists');
        }
        $geoObject->update(false);
    }
}