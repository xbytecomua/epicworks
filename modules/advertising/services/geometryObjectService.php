<?php

namespace app\modules\advertising\services;


use app\modules\advertising\geometry\Feature;
use app\modules\advertising\geometry\FeatureCollection;
use app\modules\advertising\models\GeometryObject;
use app\modules\advertising\repositories\GeometryObjectRepository;
use geoPHP\geoPHP;

class  geometryObjectService
{
    private $geometryObjectRepository;

    public function __construct(GeometryObjectRepository $objectRepository)
    {
        $this->geometryObjectRepository = $objectRepository;
    }

    public function create($geojson){
        $data = geoPHP::load($geojson,'json');
        $geometry = $data->out('wkt',true);
        $object = GeometryObject::create($geometry);
        $this->geometryObjectRepository->add($object);
        return $object;
    }

    public function import($o)
    {
        $id = $o->properties->object_id;
        $title = $o->properties->object_title;
        $point = geoPHP::load('POINT('.$o->properties->object_center[0].' '.$o->properties->object_center[1].')','wkt');
        $center = $point->out('wkt',true);
        $geometry = geoPHP::load($o->geometry,'json');
        $geometry = $geometry->out('wkt',true);

        $object = GeometryObject::import($id,$title,$center,$geometry);
        $this->geometryObjectRepository->add($object);

    }

    /**
     * Updates an existing object geometry
     * @return \app\modules\advertising\models\GeometryObject;
     */
    public function editGeometry($id, $geojson)
    {
        $model = $this->geometryObjectRepository->find($id);
        $data = geoPHP::load($geojson,'json');
        $geometry = $data->out('wkt',true);
        $model->editGeometry($geometry);
        $this->geometryObjectRepository->save($model);
        return $model;
    }

    public function editData($form)
    {
        $model = $this->geometryObjectRepository->find($form->id);
        $data = geoPHP::load('POINT('.$form->longitude.' '.$form->latitude.')','wkt');
        $center = $data->out('wkt',true);
        $model->editData($form->title, $center );
        $this->geometryObjectRepository->save($model);
        return ['id'=>$form->id, 'title'=>$form->title, 'center'=>[$form->longitude,$form->latitude]];
    }
    /**
     * Set an existing object as deleted
     * @param $id
     * @return \app\modules\advertising\models\GeometryObject;
     */
    public function deleteObject($id){
        $model = $this->geometryObjectRepository->find($id);
        $model->softDelete();
        $this->geometryObjectRepository->save($model);
        return $model;
    }

    public function listGeojson()
    {
        $list = $this->geometryObjectRepository->findAll();
        $featureCollection = new FeatureCollection();
        foreach($list as $geoObject)
        {
            $geometryWkt = geoPHP::load($geoObject->geometry, 'wkt');
            $geometry = $geometryWkt->out('json',true);
            $center = $this->getCenter($geoObject);
            $feature = Feature::create($geometry,['id'=>$geoObject->id, 'title' => $geoObject->title, 'center'=>$center]);
            $featureCollection->addFeature($feature);
        }
        return $featureCollection;
    }


    public function getObjectAsGeojson($id)
    {
        $geometryObject = $this->geometryObjectRepository->load($id);
        $featureCollection = new FeatureCollection();
        $geometryWkt = geoPHP::load($geometryObject->geometry , 'wkt');
        $geometry = $geometryWkt->out('json',true);
        $center = $this->getCenter($geometryObject);
        $feature = Feature::create($geometry,['id'=>$geometryObject->id, 'title' => $geometryObject->title, 'center'=>$center]);
        $featureCollection->addFeature($feature);

        return $featureCollection;
    }

    protected function getCenter($ob){
        $center = null;
        if($ob->center) {
            $centerWkt = geoPHP::load($ob->center, 'wkt');
            $center = [$centerWkt->getX(),$centerWkt->getY()];
        }
        return $center;
    }
}