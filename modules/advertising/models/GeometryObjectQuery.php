<?php

namespace app\modules\advertising\models;

/**
  * @see GeometryObject
 */
class GeometryObjectQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[deleted]]=0');
    }

    /**
     * {@inheritdoc}
     * @return Object[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Object|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}
