<?php

namespace app\modules\advertising\models;

use Yii;

/**
 * This is the model class for table "advertising_order".
 *
 * @property int $id
 * @property string $created_at
 * @property int $firm_id
 *
 * @property Firm $firm
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertising_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'firm_id'], 'required'],
            [['created_at'], 'safe'],
            [['firm_id'], 'integer'],
            [['firm_id'], 'exist', 'skipOnError' => true, 'targetClass' => Firm::class, 'targetAttribute' => ['firm_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('advertising', 'ID'),
            'created_at' => Yii::t('advertising', 'Created At'),
            'firm_id' => Yii::t('advertising', 'Firm ID'),
        ];
    }

    public function createOrder($firm_id)
    {
        $this->firm_id = $firm_id;
        $this->created_at = new \yii\db\Expression('NOW()');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirm()
    {
        return $this->hasOne(Firm::class, ['id' => 'firm_id']);
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
