<?php

namespace app\modules\advertising\models;

use Yii;
use yii\db\Expression;

class GeometryObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */


    public static function tableName()
    {
        return 'advertising_object';
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('advertising', 'ID'),
            'title' => Yii::t('advertising', 'Title'),
            'center' => Yii::t('advertising', 'Center'),
            'geometry' => Yii::t('advertising', 'Geometry'),
            'deleted' => Yii::t('advertising', 'Deleted'),
        ];
    }
    /**
     * {@inheritdoc}
     * @return GeometryObjectQuery the active query used by this AR class.
     */

    public static function find()
    {
        return new GeometryObjectQuery(get_called_class());
    }

    public static function create($geometry)
    {
        $model = new GeometryObject();
        $model->geometry = new Expression("ST_GeometryFromText(:geoText)", [':geoText' =>"$geometry"]);
        $model->deleted = false;
        return $model;
    }
    public static function import($id,$title,$center,$geometry)
    {
        $model = new GeometryObject();
        $model->id = $id;
        $model->title = $title;
        $model->center = new Expression("ST_GeometryFromText(:geoCenter)", [':geoCenter' =>"$center"]);
        $model->geometry = new Expression("ST_GeometryFromText(:geometry)", [':geometry' =>"$geometry"]);
        $model->deleted = false;
        return $model;
    }

    public function editGeometry($geometry)
    {
        $this->geometry = new Expression("ST_GeometryFromText(:geoText)", [':geoText' =>"$geometry"]);
        return $this;
    }

    public function editData($title, $center)
    {
        $this->title = $title;
        $this->center =  new Expression("ST_GeometryFromText(:geoText)", [':geoText' =>"$center"]);
        return $this;
    }

    public function softDelete(){
        $this->deleted = true;
        return $this;
    }

}
