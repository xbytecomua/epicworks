<?php

namespace app\modules\advertising\models;

/**
 * This is the ActiveQuery class for [[FisAddress]].
 *
 * @see FisAddress
 */
class FisAddressQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FisAddress[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FisAddress|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
