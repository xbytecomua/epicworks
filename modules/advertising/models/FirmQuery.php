<?php

namespace app\modules\advertising\models;

/**
 * This is the ActiveQuery class for [[AdvertisingFirma]].
 *
 * @see AdvertisingFirma
 */
class FirmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Firm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Firm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
