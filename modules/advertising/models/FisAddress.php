<?php

namespace app\modules\advertising\models;

use Yii;

/**
 * This is the model class for table "fis_address".
 *
 * @property int $id
 * @property string $nba
 * @property string $oi
 * @property string $qua
 * @property int $lan
 * @property int $rbz
 * @property int $krs
 * @property int $gmd
 * @property int $ott
 * @property int $sss
 * @property int $hnr
 * @property int $adz
 * @property string $point
 * @property string $stn
 * @property int $plz
 * @property string $onm
 * @property string $zon
 * @property string $pot
 * @property string $psn
 * @property string $aud
 */
class FisAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fis_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nba', 'oi', 'qua', 'lan', 'rbz', 'krs', 'gmd', 'ott', 'sss', 'hnr', 'adz', 'point', 'stn', 'plz', 'onm', 'zon', 'pot', 'psn', 'aud'], 'required'],
            [['lan', 'rbz', 'krs', 'gmd', 'ott', 'sss', 'hnr', 'adz', 'plz'], 'integer'],
            [['point'], 'string'],
            [['aud'], 'safe'],
            [['nba'], 'string', 'max' => 1],
            [['oi'], 'string', 'max' => 50],
            [['qua', 'zon', 'pot', 'psn'], 'string', 'max' => 10],
            [['stn', 'onm'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('advertising', 'ID'),
            'nba' => Yii::t('advertising', 'Nba'),
            'oi' => Yii::t('advertising', 'Oi'),
            'qua' => Yii::t('advertising', 'Qua'),
            'lan' => Yii::t('advertising', 'Lan'),
            'rbz' => Yii::t('advertising', 'Rbz'),
            'krs' => Yii::t('advertising', 'Krs'),
            'gmd' => Yii::t('advertising', 'Gmd'),
            'ott' => Yii::t('advertising', 'Ott'),
            'sss' => Yii::t('advertising', 'Sss'),
            'hnr' => Yii::t('advertising', 'Hnr'),
            'adz' => Yii::t('advertising', 'Adz'),
            'point' => Yii::t('advertising', 'Point'),
            'stn' => Yii::t('advertising', 'Stn'),
            'plz' => Yii::t('advertising', 'Plz'),
            'onm' => Yii::t('advertising', 'Onm'),
            'zon' => Yii::t('advertising', 'Zon'),
            'pot' => Yii::t('advertising', 'Pot'),
            'psn' => Yii::t('advertising', 'Psn'),
            'aud' => Yii::t('advertising', 'Aud'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return FisAddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FisAddressQuery(get_called_class());
    }
}
