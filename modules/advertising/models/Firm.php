<?php

namespace app\modules\advertising\models;

use Yii;

/**
 * This is the model class for table "advertising_firma".
 *
 * @property int $id
 * @property string $title
 *
 * @property Order[] $advertisingOrders
 */
class Firm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertising_firm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('advertising', 'ID'),
            'title' => Yii::t('advertising', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisingOrders()
    {
        return $this->hasMany(Order::className(), ['firm_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return FirmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FirmQuery(get_called_class());
    }
}
