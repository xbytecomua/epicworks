<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * LeafletRestoreView asset bundle.
 *
 */
class LeafletRestoreViewAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/advertising/resources';
    public $js = [
        'js/leaflet-restore-view.js'
    ];

    public $depends = [

    ];
}
