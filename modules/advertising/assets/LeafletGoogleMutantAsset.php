<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * LeafletGoogleMutantAsset asset bundle.
 */
class LeafletGoogleMutantAsset extends AssetBundle
{
    public $sourcePath = '@npm/leaflet.gridlayer.googlemutant';

    public $js = [
        'Leaflet.GoogleMutant.js'
    ];
    public $publishOptions = [
        'only' => [
            'Leaflet.GoogleMutant.js'
        ]
    ];
    public $depends = [
        'app\modules\advertising\assets\GoogleMapsAsset'
    ];
}
