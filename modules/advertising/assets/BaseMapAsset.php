<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * BaseMap asset bundle.
 *
 */
class BaseMapAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/advertising/resources';
    public $css = [
        'css/map.css'
    ];
    public $js = [
        'js/basemap.js'
    ];

    public $depends = [
        'app\modules\advertising\assets\LeafletAsset',
        'app\modules\advertising\assets\LeafletBingLayerAsset',
        'app\modules\advertising\assets\LeafletRestoreViewAsset',
        'app\modules\advertising\assets\LeafletGoogleMutantAsset',
        'app\modules\advertising\assets\LeafletContextMenuAsset',
        //'app\modules\advertising\assets\LeafletMarkerClusterAsset'
        'app\modules\advertising\assets\LeafletFileLayerAsset',
    ];
}
