<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * Leafletjs asset bundle.
 *
 */
class LeafletAsset extends AssetBundle
{
    public $sourcePath = '@bower/leaflet/dist';
    public $css = [
        'leaflet.css'
    ];
    public $js = [
        'leaflet.js'
    ];
}
