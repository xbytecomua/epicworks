<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * LeafletDraw asset bundle.
 *
 */
class LeafletDrawAsset extends AssetBundle
{
    public $sourcePath = '@bower/leaflet.draw/dist';
    public $css = [
        'leaflet.draw.css'
    ];
    public $js = [
        'leaflet.draw.js'
    ];

    public $publishOptions = [
        'except' => [
            'leaflet.draw-src.css',
            'leaflet.draw-src.js',
        ]
    ];
    public $depends = [

    ];
}
