<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * LeafletDraw asset bundle.
 *
 */
class LeafletPMAsset extends AssetBundle
{
    public $sourcePath = '@bower/leaflet.pm/dist';
    public $css = [
        'leaflet.pm.css'
    ];
    public $js = [
        'leaflet.pm.min.js'
    ];

//    public $publishOptions = [
//        'except' => [
//            'leaflet.draw-src.css',
//            'leaflet.draw-src.js',
//        ]
//    ];
    public $depends = [

    ];
}
