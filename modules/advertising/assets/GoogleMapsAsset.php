<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use Yii;
use yii\web\AssetBundle;
use yii\base\ErrorException;

/**
 * GoogleMapsAsset asset bundle.
 *
 */
class GoogleMapsAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [

    ];
    /**
     * @var string Google Developers API key
     * @link https://developers.google.com/maps/documentation/javascript/get-api-key
     */
    public $apiKey = 'AIzaSyCLI1vgwiUWROv83DPHZdte3TSqGTrjMjU';

    public function init(){
        parent::init();
        if($this->apiKey === null){
            throw new ErrorException("You must configure GoogleMapsAsset. See README.md for details.");
        }
        $this->js = [
            '//maps.google.com/maps/api/js?key='.$this->apiKey.'&language=de',
        ];
    }
}

