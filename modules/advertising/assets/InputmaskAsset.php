<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * RobinHerbots Inputmask asset bundle.
 *
 */
class InputmaskAsset extends AssetBundle
{
    public $sourcePath = '@npm/inputmask/dist/min/inputmask/';
    public $css = [

    ];
    public $js = [
        'inputmask.min.js',
        'jquery.inputmask.min.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
