<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * Tinyqueue asset bundle.
 *
 */
class TinyqueueAsset extends AssetBundle
{
    public $sourcePath = '@npm/tinyqueue';
    public $css = [

    ];
    public $js = [
        'tinyqueue.js'
    ];
}
