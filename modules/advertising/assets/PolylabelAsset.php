<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * Polylabel asset bundle.
 *
 */
class PolylabelAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/advertising/resources';
    public $css = [

    ];
    public $js = [
        'js/polylabel.js'
    ];

    public $depends = [
        'app\modules\advertising\assets\TinyqueueAsset',
    ];
}
