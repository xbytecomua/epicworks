<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * BaseMap asset bundle.
 *
 */
class LeafletFileLayerAsset extends AssetBundle
{
    public $sourcePath = '@npm/leaflet-filelayer';
    public $css = [

    ];
    public $js = [
        'src/leaflet.filelayer.js',

    ];

    public $depends = [
        'app\modules\advertising\assets\MapboxToGeojsonAsset',
    ];
}
