<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * MapEditor asset bundle.
 *
 */
class MapPrintAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/advertising/resources';
    public $css = [

    ];
    public $js = [
        'js/print.js'
    ];

    public $depends = [
        'app\modules\advertising\assets\BaseMapAsset',
    ];
}