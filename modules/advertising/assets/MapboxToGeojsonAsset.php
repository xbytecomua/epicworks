<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * BaseMap asset bundle.
 *
 */
class MapboxToGeojsonAsset extends AssetBundle
{
    public $sourcePath = '@npm/togeojson';
    public $css = [

    ];
    public $js = [
        'togeojson.js',

    ];

    public $depends = [

    ];
}
