<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * BaseMap asset bundle.
 *
 */
class LeafletEasyPrintAsset extends AssetBundle
{
    public $sourcePath = '@npm/leaflet-easyprint';
    public $css = [

    ];
    public $js = [
        'dist/bundle.js'
    ];

    public $depends = [

    ];
}
