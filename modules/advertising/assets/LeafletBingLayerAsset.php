<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\advertising\assets;

use yii\web\AssetBundle;

/**
 * LeafletBingLayer asset bundle.
 */
class LeafletBingLayerAsset extends AssetBundle
{
    public $sourcePath = '@bower/leaflet-bing-layer';

    public $js = [
        'leaflet-bing-layer.js'
    ];
    public $publishOptions = [
        'only' => [
            'leaflet-bing-layer.js'
        ]
    ];
    public $depends = [

    ];
}
