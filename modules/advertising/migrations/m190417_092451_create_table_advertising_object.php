<?php

use yii\db\Migration;

class m190417_092451_create_table_advertising_object extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }

        $this->createTable('{{%advertising_object}}', [
            'id' => $this->primaryKey(11),
            'title' => $this->string(8),
            'center' => 'POINT',
            'geometry' => 'POLYGON NOT NULL',
            'deleted' => $this->boolean()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex(
            'idx-object-title',
            'advertising_object',
            'title'
        );
        $this->createIndex(
            'idx-object-deleted',
            'advertising_object',
            'deleted'
        );

        if ($this->db->driverName === 'mysql') {
            $this->execute('CREATE SPATIAL INDEX `idx-geometry` ON '.'{{%advertising_object}}(geometry);');
        } elseif ($this->db->driverName === 'pgsql') {
            $this->execute('CREATE INDEX "idx-geometry" ON '.'{{%advertising_object}} USING GIST(geometry);');
        }

    }

    public function down()
    {
        $this->dropTable('{{%advertising_object}}');
    }
}
