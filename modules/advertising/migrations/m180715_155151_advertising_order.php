<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180715_155151_advertising_order
 */
class m180715_155151_advertising_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advertising_order}}', [
            'id' => Schema::TYPE_PK,
            'created_at' => Schema::TYPE_DATE . ' NOT NULL',
            'firm_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createIndex(
            'idx-firm_id',
            '{{%advertising_order}}',
            'firm_id'
        );

        $this->addForeignKey(
            'fk-order-firm_id',
            '{{%advertising_order}}',
            'firm_id',
            '{{%advertising_firm}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-order-firm_id',
            '{{%advertising_order}}'
        );

        $this->dropTable('{{%advertising_order}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180715_155151_advertising_order cannot be reverted.\n";

        return false;
    }
    */
}
