<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180715_155132_advertising_firma
 */
class m180715_155132_advertising_firm extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advertising_firm}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advertising_firm}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180715_155132_advertising_firma cannot be reverted.\n";

        return false;
    }
    */
}
