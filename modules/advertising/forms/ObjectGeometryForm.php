<?php

namespace app\modules\advertising\forms;

use Yii;
use yii\base\Model;

class ObjectGeometryForm extends Model
{
    public $geojson;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // geometry required
            ['geojson', 'required'],
            ['geojson', 'validateGeojson'],
        ];
    }

    public function validateGeojson($attribute)
    {
        if(!$jsonObject = json_decode($this->$attribute)){
            $this->addError($attribute, json_last_error());
        }
        else if(!is_object($jsonObject) || !$jsonObject->geometry->type || !is_array($jsonObject->geometry->coordinates))
        {
            $this->addError($attribute,'Wrong Geojson');
        }
    }
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'geojson' => 'geojson',
        ];
    }

}
