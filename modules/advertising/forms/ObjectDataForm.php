<?php

namespace app\modules\advertising\forms;
use Yii;
use yii\base\Model;

class ObjectDataForm extends Model
{
    public $id;
    public $title;
    public $longitude;
    public $latitude;

    /**
     * @return array the validation rules.
     */

    public function rules()
    {
        return [
            [['id','title','longitude','latitude'], 'required'],
            ['id', 'integer'],
            ['title', 'string', 'length' => [7, 8]],
            [['longitude','latitude'],'integer', 'integerPattern' => '/^[+-]?[0-9]{1,3}(\.[0-9]{4,})$/', 'message' => Yii::t('advertising', 'Wrong coordinate format'),],
            [
                'title',
                'unique',
                'targetClass' => '\app\modules\advertising\models\GeometryObject',
                'targetAttribute' => ['title'],
                //'filter' => ['!=', 'id', $this->getId()],
                'filter' => function ($query) {

                    $query->andWhere(['!=','id', $this->getId()]);
                    $query->andWhere(['!=','deleted', true]);

            }
            ]
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array customized attribute labels
     */

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('advertising', 'Region Name'),
            'longitude'=> Yii::t('advertising', 'Longitude'),
            'latitude'=>Yii::t('advertising', 'Latitude'),
        ];
    }

}
