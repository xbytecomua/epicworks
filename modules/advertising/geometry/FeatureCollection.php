<?php

namespace app\modules\advertising\geometry;
use app\modules\advertising\geometry\Feature;


class FeatureCollection implements \JsonSerializable
{
    const Type = 'FeatureCollection';
    protected $features = [];

    public function addFeature(Feature $feature){
        array_push($this->features, $feature);
    }
    public function jsonSerialize() {
        return [
            'type' => self::Type,
            'features' => $this->features
        ];
    }
}