<?php

namespace app\modules\advertising\geometry;


class Feature implements \JsonSerializable
{
    const Type = 'Feature';
    protected $properties;
    protected $geometry = [];

    public static function create($geometry, $properties = '{}') {
        $feature = new self();
        $feature->geometry = $geometry;
        $feature->properties = $properties;
        return $feature;
    }


    public function jsonSerialize() {
        return [
            'type' => self::Type,
            'properties' => $this->properties,
            'geometry' => $this->geometry
        ];
    }
}