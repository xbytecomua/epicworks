<?php

//use app\modules\advertising\assets\MapEditorPMAsset;
use yii\web\View;
use yii\helpers\Url;
use \app\modules\advertising\assets\MapEditorPMAsset;

MapEditorPMAsset::register($this);
$actions = [
    'create' => Url::to('object/create'),
    'update' => Url::to('object/update'),
    'list' => Url::to('object/list'),
];

$this->registerJs(
    "var actions = ".\yii\helpers\Json::htmlEncode($actions).";",
    View::POS_HEAD,
    'editorConfig'
);
?>
<div id="map"></div>

<? $this->beginBlock('control-sidebar');?>
    <!-- Control Sidebar -->
    <aside class="control-sidebar">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" id="exampleInputFile">

                        <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Check me out
                        </label>
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
    <!--<div class="control-sidebar-bg"></div>-->
<? $this->endBlock();?>