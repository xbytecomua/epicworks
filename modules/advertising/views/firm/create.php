<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\advertising\models\Firm */

$this->title = Yii::t('advertising', 'Create Advertising Firm');
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertising', 'Advertising Firms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertising-firm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
