<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\advertising\models\Firm */

$this->title = Yii::t('advertising', 'Update Advertising Firm: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertising', 'Advertising Firms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('advertising', 'Update');
?>
<div class="advertising-firm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
