<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\modules\advertising\assets\LeafletContextMenuAsset;
use \app\modules\advertising\assets\MapEditorAsset;
use \app\modules\advertising\assets\InputmaskAsset;

MapEditorAsset::register($this);
InputmaskAsset::register($this);

$contextMenuPath = $this->assetManager->getBundle(LeafletContextMenuAsset::class)->baseUrl;

$this->registerJs(
    "var contextMenuPath = '". $contextMenuPath ."'",
    View::POS_HEAD,
    'contextMenuAssets'
);

$this->registerJs(
    'jQuery(document).ready(function(){
        jQuery("#objectdataform-title").inputmask( "99999-9[9]",{ "placeholder": "" });
    });
    '
);

$actions = [
    'create' => Url::to('/advertising/object/create'),
    'editGeometry' => Url::to('/advertising/object/edit-geometry'),
    'editData' => Url::to('/advertising/object/edit-data'),
    'delete'=> Url::to('/advertising/object/delete'),
    'list' => Url::to('/advertising/object/list'),
    'print'=> Url::to('/advertising/map-editor/print'),
];

$this->registerJs(
    "var actions = ".\yii\helpers\Json::htmlEncode($actions).";",
    View::POS_HEAD,
    'editorConfig'
);

$translate = [
    'showCoordinates' => Yii::t('advertising', 'Show Coordinates'),
    'centerMap' => Yii::t('advertising', 'Center map here'),
    'zoomIn' => Yii::t('advertising', 'Zoom In'),
    'zoomOut' => Yii::t('advertising', 'Zoom Out'),
    'changeRegionConfiguration'=> Yii::t('advertising', 'Change region configuration'),
    'print' => Yii::t('advertising', 'Print'),
    'loading' => Yii::t('advertising', 'Loading'),
];

$this->registerJs(
    "var translateStrings = ".\yii\helpers\Json::htmlEncode($translate).";",
    View::POS_HEAD,
    'translateStrings'
);


?>
<div id="map"></div>
<? $this->beginBlock('control-sidebar');?>
    <!-- Control Sidebar -->
    <aside class="control-sidebar">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('advertising', 'Change region configuration'); ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" id="control-sidebar" class="btn btn-box-tool" data-toggle="control-sidebar"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = \yii\widgets\ActiveForm::begin([
                'id' => 'region-data',
                'enableClientValidation'=> false,
                'enableAjaxValidation' => true,
                'validationUrl' => Url::to('/advertising/object/validate-edit-data'),
                'action' => Url::to('/advertising/object/edit-data'),

            ]); ?>
                <div class="box-body">
                    <?= $form->field($validateForm, 'id')->hiddenInput(['value'=> ''])->label(false); ?>
                    <?= $form->field($validateForm, 'title')->textInput(['class' => 'form-control input-sm']) ?>
                    <?= $form->field($validateForm, 'longitude')->textInput(['class' => 'form-control input-sm', 'placeholder'=> 'test placeholder']) ?>
                    <?= $form->field($validateForm, 'latitude')->textInput(['class' => 'form-control input-sm','placeholder'=> 'test placeholder']) ?>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <?= Html::submitButton(Yii::t('advertising', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>





        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
    <!--<div class="control-sidebar-bg"></div>-->
<? $this->endBlock();?>