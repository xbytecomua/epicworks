<?php

use yii\web\View;

$this->registerJs(
"
function load(start=1,bytes=0) {
var url = 'fis-import?from='+start+'&bytes='+bytes;
    var timer = setTimeout(function () {
        $.ajax({
            url: url,
            type: 'GET',
  
            success: function (result) {
                if(result.next == 'null')
                {
                    stopTimer();
                    $('.progress-bar').css('width', '100%').attr('aria-valuenow', 100);
                }
                else{
                    load(result.next,result.bytes);
                    $('.progress-bar').css('width', result.percent+'%').attr('aria-valuenow', result.percent);
                }
            },
        });
    }, 3000);
    function stopTimer(){
        clearInterval(timer);
    }
}
load();
",
View::POS_READY,
'progressbar'
);

?>
<div class="progress">
    <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
        <span class="sr-only"></span>
    </div>
</div>
