<?php

use yii\web\View;
use \app\modules\advertising\assets\LeafletContextMenuAsset;
use \app\modules\advertising\assets\BaseMapAsset;
use \app\modules\advertising\assets\MapPrintAsset;
use \app\modules\advertising\assets\LeafletEasyPrintAsset;

BaseMapAsset::register($this);
LeafletContextMenuAsset::register($this);
LeafletEasyPrintAsset::register($this);
MapPrintAsset::register($this);


$contextMenuPath = $this->assetManager->getBundle(LeafletContextMenuAsset::class)->baseUrl;

$this->registerJs(
    "let contextMenuPath = '". $contextMenuPath ."'",
    View::POS_HEAD,
    'contextMenuAssets'
);

$translate = [
    'showCoordinates' => Yii::t('advertising', 'Show Coordinates'),
    'centerMap' => Yii::t('advertising', 'Center map here'),
    'zoomIn' => Yii::t('advertising', 'Zoom In'),
    'zoomOut' => Yii::t('advertising', 'Zoom Out'),
];

$this->registerJs(
    "let translateStrings = ".\yii\helpers\Json::htmlEncode($translate).";",
    View::POS_HEAD,
    'translateStrings'
);

$this->registerJs(
    "let translateStrings = ".\yii\helpers\Json::htmlEncode($translate).";",
    View::POS_HEAD,
    'translateStrings'
);

$this->registerJs(
    "let geoJson = ".\yii\helpers\Json::htmlEncode($geoJson).";",
    View::POS_HEAD,
    'geoJson'
);
?>
<div class="print-wrapper">
    <div id="map" class="map-print"></div>
    <table id="table">
        <tbody>
        <tr>
            <td>Datum:</td>
            <td class="empty-big"></td>
            <td>Zeit:</td>
            <td class="empty-small"></td>
            <td>Bis:</td>
            <td class="empty-small"></td>
            <td>Datum:</td>
            <td class="empty-big"></td>
            <td>Zeit:</td>
            <td class="empty-small"></td>
            <td>Bis:</td>
            <td class="empty-small"></td>
        </tr>
        <tr>
            <td>Name:</td>
            <td class="empty-big username"></td>
            <td>Anzahl:</td>
            <td class="empty-small"></td>
            <td>%</td>
            <td class="empty-small"></td>
            <td>Name:</td>
            <td class="empty-big username"></td>
            <td>Anzahl:</td>
            <td class="empty-small"></td>
            <td>%</td>
            <td class="empty-small"></td>
        </tr>
        </tbody>
    </table>
</div>