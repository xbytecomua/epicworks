<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\advertising\models\Order */

$this->title = Yii::t('advertising', 'Create Advertising Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertising', 'Advertising Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertising-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
