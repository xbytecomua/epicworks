<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\advertising\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advertising-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'firm_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('advertising', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
