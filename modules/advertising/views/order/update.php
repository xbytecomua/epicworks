<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\advertising\models\Order */

$this->title = Yii::t('advertising', 'Update Advertising Order: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertising', 'Advertising Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('advertising', 'Update');
?>
<div class="advertising-order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
