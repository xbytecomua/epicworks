<?php
    use yii\helpers\Html;
    use yii\web\View;

$params = [
    'data' => [\yii::$app->request->csrfParam => \yii::$app->request->csrfToken]
];
$this->registerJs(
    "$.ajaxSetup(".\yii\helpers\Json::htmlEncode($params).");",
    View::POS_READY,
    '_csrf'
);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
