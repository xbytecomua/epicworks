<?php

use yii\helpers\Html;
use yii\bootstrap\Button;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\View;
use yii\grid\ActionColumn;
/* @var $this yii\web\View */
/* @var $firmModel app\modules\advertising\models\Firm */
/* @var $firmSearchModel app\modules\advertising\models\FirmSearch */
/* @var $firmDataProvider yii\data\ActiveDataProvider */
/* @var $orderSearchModel app\modules\advertising\models\OrderSearch */
/* @var $orderDataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('advertising', 'Advertising management');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$this->registerJs(
    "function firmClick(row,firmId){
        $(row).addClass('active').siblings().removeClass('active');
        $.pjax.reload(
            {
              method: 'POST',
              container: '#order_pjax',
              data: {'OrderSearch[firm_id]': firmId},
            }
        );
        var link = $('#create-order-button'); 
        link.attr('href', link.attr('href') + '?firmid=' + firmId);
        link.removeClass( 'disabled' )
    }",
    View::POS_BEGIN
);
?>
<div class="advertising-management-index">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title">
                        <?php echo \Yii::t('advertising', 'Firms') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table table-bordered">
                    <?php
                    Pjax::begin(
                        (
                        ['id' => 'firm_pjax',
                            'timeout' => false,
                            'enablePushState' => true,
                            'clientOptions' => ['method' => 'POST']]
                        )
                    );
                    ?>
                    <?= GridView::widget([
                        'dataProvider' => $firmDataProvider,
                        'filterModel' => $firmSearchModel,
                        'showHeader'=> false,
                        'showOnEmpty'=>false,
                        'summary' =>'',
                        'tableOptions' => ['class' => 'table table-bordered table-responsive'],
                        'rowOptions' => function ($model, $index, $widget, $grid) {
                            return [
                                'onclick' => "firmClick(this,".$model['id'].");",
                            ];
                        },
                        'columns' => [
                            'title',
                            [
                                'class'    => 'yii\grid\ActionColumn',
                                'template' => '{firmEdit} {firmDelete}',
                                'buttons'  => [
                                    'firmEdit' => function ($url, $model) {
                                        $url = Url::to(['/advertising/firm/update', 'id' => $model->id]);
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                            'title'        => 'edit',
                                            'data-pjax' => '0'
                                        ]);
                                    },
                                    'firmDelete' => function ($url, $model) {
                                        $url = Url::to(['/advertising/firm/delete', 'id' => $model->id]);
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title'        => 'delete',
                                            'data-confirm' => Yii::t('advertising', 'Are you sure you want to delete this item?'),
                                            'data-method'  => 'post',
                                        ]);
                                    },
                                ]
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                    <?= Html::a(Yii::t('advertising', 'New Firm'), Url::to(['/advertising/firm/create']), ['class'=>'btn btn-success pull-right']) ?>

                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title"><?php echo \Yii::t('advertising', 'Orders') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-bordered ">
                    <?php
                        Pjax::begin(
                            (
                                ['id' => 'order_pjax',
                                'timeout' => false,
                                'enablePushState' => true,
                                'clientOptions' => ['method' => 'POST']]
                            )
                        );
                    ?>
                    <?= GridView::widget([
                        'dataProvider' => $orderDataProvider,
                        'filterModel' => $orderSearchModel,
                        'showHeader'=> false,
                        'showOnEmpty'=>false,
                        'summary' =>'',
                        'tableOptions' => ['class' => 'table table-bordered table-responsive'],
                        'columns' => [
                            'firm.title',
                            'created_at',
                            [
                                'class'    => 'yii\grid\ActionColumn',
                                'template' => '{orderDelete}',
                                'buttons'  => [
                                    'orderDelete' => function ($url, $model) {
                                        $url = Url::to(['/advertising/order/delete', 'id' => $model->id]);
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title'        => 'delete',
                                            'data-confirm' => Yii::t('advertising', 'Are you sure you want to delete this item?'),
                                            'data-method'  => 'post',
                                        ]);
                                    },
                                ]
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                    <?= Html::a(Yii::t('advertising', 'New Order'), Url::to(['/advertising/order/create']), ['id'=>'create-order-button', 'class'=>'btn disabled btn-success pull-right']) ?>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>

