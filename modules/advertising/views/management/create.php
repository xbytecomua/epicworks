<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\advertising\models\AdvertisingFirma */

$this->title = Yii::t('advertising', 'Create Advertising Firma');
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertising', 'Advertising Firmas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertising-firma-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
