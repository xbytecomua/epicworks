<?php

return [
    'Save' => 'Сохранить',
    'Loading' => 'Загрузка',
    'Firma'=>'Фирма',
    'Firms'=>'Фирмы',
    'New Firm'=>'Новая Фирма',
    'Order'=>'Заказ',
    'Orders'=>'Заказы',
    'New Order'=>'Новый Заказ',
    'Create new order for'=>'Создать новый заказ для',
    'Show Coordinates'=> 'Показать координаты',
    'Center map here' => 'Центрировавть карту здесь',
    'Zoom In' => 'Приблизить',
    'Zoom Out' => 'Отдалить',
    'Change region configuration' => 'Изменить настройки района',
    'Print' => 'Печать',
    'Region Name' =>  'Название Района',
    'Longitude' => 'Долгота',
    'Latitude' => 'Широта',
    'Wrong coordinate format' => 'Неправильный формат координат',
];