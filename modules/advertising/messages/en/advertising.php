<?php

return [
    'Save' => 'Save',
    'Loading' => 'Loading',
    'Firma' => 'Firma',
    'Firms' => 'Firms',
    'New Firm' => 'Neue Firma',
    'Order' => 'Order',
    'Orders' => 'Orders',
    'New Order' => 'New Order',
    'Create new order for' => 'Create new order for',
    'Show Coordinates' => 'Show Coordinates',
    'Center map here' => 'Center map here',
    'Zoom In' => 'Zoom In',
    'Zoom Out' => 'Zoom Out',
    'Change region configuration' => 'Change region configuration',
    'Print' => 'Print',
    'Region Name' => 'Region Name',
    'Longitude' => 'Longitude',
    'Latitude' => 'Latitude',
    'Wrong coordinate format' => 'Falsches Koordinatenformat',
];
