<?php

return [
    'Save' => 'Speichern',
    'Loading' => 'Loading',
    'Firma'=>'Firma',
    'Firms'=>'Firmen',
    'New Firm'=>'Neue Firma',
    'Order'=>'Auftrag',
    'Orders'=>'Aufträge',
    'New Order'=>'Neue Auftrag',
    'Create new order for'=>'Erstellen neuer Auftrag für',
    'Show Coordinates'=> 'Zeige Koordinaten',
    'Center map here' => 'Karte hier zentrieren',
    'Zoom In' => 'Hineinzoomen',
    'Zoom Out' => 'Rauszoomen',
    'Change region configuration' => 'Region ändern Konfiguration',
    'Print' => 'Drucken',
    'Region Name' =>  'Region Name',
    'Longitude' => 'Longitude',
    'Latitude' => 'Latitude',
    'Wrong coordinate format' => 'Falsches Koordinatenformat',
];
