<?php

namespace app\modules\advertising\controllers;

use Yii;
use yii\helpers\Url;
use app\modules\advertising\models\Firm;
use app\modules\advertising\models\FirmSearch;
use app\modules\advertising\models\OrderSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
/**
 * Default controller for the `advertising` module
 */
class ManagementController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        Url::remember();
        $firmModel = new Firm();
        $firmSearchModel = new FirmSearch();
        $firmDataProvider = $firmSearchModel->search(Yii::$app->request->post());
        $orderSearchModel = new OrderSearch();
        $orderDataProvider = $orderSearchModel->search(Yii::$app->request->post());

        return $this->render('index', [
            'firmModel' => $firmModel,
            'firmSearchModel' => $firmSearchModel,
            'firmDataProvider' => $firmDataProvider,
            'orderSearchModel' => $orderSearchModel,
            'orderDataProvider' => $orderDataProvider,
        ]);
    }
}