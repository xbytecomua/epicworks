<?php

namespace app\modules\advertising\controllers;

use app\modules\advertising\forms\ObjectDataForm;
//use app\modules\advertising\models\FisAddress;
use app\modules\advertising\models\GeometryObject;
use app\modules\advertising\services\geometryObjectService;
use PHPCoord\UTMRef;
use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * OrderController implements the CRUD actions for AdvertisingOrder model.
 */
class MapEditorController extends Controller
{
    private $geometryObjectService;

    public function __construct($id, $module, geometryObjectService $geometryObjectService, $config = [])
    {
        $this->geometryObjectService = $geometryObjectService;
        parent::__construct($id, $module, $config = []);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new AdvertisingOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionIndex()
    {
        $validateForm = new ObjectDataForm();
        return $this->render('index', ['validateForm' => $validateForm]);
    }

    public function actionPrint($id)
    {
        $this->layout = 'blank';
        $geoJson = $this->geometryObjectService->getObjectAsGeojson($id);
        return $this->render('print', ['geoJson' => $geoJson]);
    }

    public function actionFisAddress()
    {
        return $this->render('fis_loading');
    }

    public function actionFisImport($from=1,$bytes=0)
    {

        //mysql -u root -p epicworks --default-character-set=utf8 < D:\Openserver\OpenServer\domains\EpicWorks\web\HKO_2018_EPSG5650_1-100000.sql
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $strings = [];
        ////////////////////////
        $importfile = "HKO_2018_EPSG5650.txt";
        $ex = explode('.',$importfile);
        $filename = $ex[0];

        ////////////////////////
        $limit = 100000;
        $path =  Yii::getAlias('@webroot');
        $file = new \SplFileObject($path."/".$importfile,"r");
        $filesize = $file->getSize();
        $file->setFlags(\SplFileObject::READ_CSV |
            \SplFileObject::SKIP_EMPTY |
            \SplFileObject::READ_AHEAD);
        $file->setCsvControl(';');
        $file->seek($from);
        while((($from + $limit) > $file->key()) && $file->valid())
        {
            $row = $file->current();

            $bytes += strlen(implode(';', $row));
            $row = array_map([$this,'encode'], $row);
            $this->replaceUtmWithPoint($row);
            $this->addquotes($row);
            $this->addMysqlExpression($row,11,'ST_GeomFromText');
            $strings[] = implode(",", $row);
            $file->next();
        }
        $sqlFileName = $this->createImportFileName($path,$filename,$from,$file->key());
        $stop = !$file->valid() ? 'null' : $file->key();
        $file=null;
        $sql = $this->createSqlImportQuery($strings);
        $this->saveImportFile($sqlFileName,$sql);
        return ['next'=>$stop,'percent'=>$this->getProgress($filesize,$bytes),'bytes'=>$bytes,'full'=>$filesize];
    }

    protected function createImportFileName($path, $filename, $start, $stop)
    {
        return $path."/".$filename."_".$start."-".($stop-1);
    }

    protected function utm2wtk($e,$n){
        $UTM = new UTMRef(substr($e, 2),$n, 0 ,'N', 33);//$x, $y, $z, $latZone, $lngZone
        $LatLng = $UTM->toLatLng();
        return "Point(".$LatLng->getLng()." ".$LatLng->getLat().")";
    }

    protected function encode($string)
    {
        return mb_convert_encoding($string, "UTF-8", "ISO-8859-1");
    }
    protected function getStringSize()
    {

    }
    protected function replaceUtmWithPoint(&$array){
        $wkt = $this->utm2wtk($array[11],$array[12]);//e11 n 12
        $array [11] = $wkt;
        unset($array [12]);
    }

    protected function addquote($sting){
        return "'".$sting."'";
    }

    protected function addquotes(&$array){
        $array = array_map([$this, 'addquote'],$array);
    }

    protected function addMysqlExpression(&$array,$key, $exp)
    {
        $array[$key]=$exp.'('.$array[$key].')';
    }

    protected function addParentheses($string)
    {
        return "(".$string.")";
    }

    protected function getValues($strings)
    {
        $strings = array_map([$this,'addParentheses'],$strings);
        return implode(", ", $strings);
    }

    protected function createSqlImportQuery($strings)
    {
        $database = "epicworks.fis_address";
        $fields = "`nba`, `oi`, `qua`, `lan`, `rbz`, `krs`, `gmd`, `ott`, `sss`, `hnr`, `adz`, `point`, `stn`, `plz`, `onm`, `zon`, `pot`, `psn`, `aud`";
        $fields = $this->addParentheses($fields);
        $values = $this->getValues($strings);
        $sql = "INSERT INTO" . " " . $database."". $fields." VALUES ".$values.";";
        return $sql;
    }

    protected function saveImportFile($filename,$data)
    {
        $handle = fopen($filename.".sql", "w+");
        fputs($handle,$data);
        fclose($handle);
    }

    protected function  getProgress($filesize,$bytes){
        return ($bytes/$filesize)*100;
    }

    protected function findModel($id)
    {
        if (($model = GeometryObject::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('advertising', 'The requested page does not exist.'));
    }
}

//INSERT INTO epicworks.fis_address
//SELECT `id`, `nba`, `oi`, `qua`, `lan`, `rbz`, `krs`, `gmd`, `ott`, `sss`, `hnr`, `adz`, ST_GeomFromtext(`point`) as point, `stn`, `plz`, `onm`, `zon`, `pot`, `psn`, `aud`FROM CSV_DB.TBL_NAME