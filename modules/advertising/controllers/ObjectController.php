<?php

namespace app\modules\advertising\controllers;

use app\modules\advertising\forms\ObjectDataForm;
use app\modules\advertising\forms\ObjectGeometryForm;
use app\modules\advertising\models\GeometryObject;
use app\modules\advertising\services\geometryObjectService;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;


class ObjectController extends Controller
{
    private $geometryObjectService;

    public function __construct($id, $module, geometryObjectService $geometryObjectService, $config = [])
    {
        $this->geometryObjectService = $geometryObjectService;
        parent::__construct($id, $module, $config = []);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create'=>['POST','GET'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $form = new ObjectGeometryForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $model = $this->geometryObjectService->create($form->geojson);
            return ['data'=>['id'=>$model->id],'status'=>'success'];
            //TODO: add try catch error handler here
        }

    }

    public function actionImportOldDatabase()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $path =  Yii::getAlias('@webroot');

        $json =json_decode( file_get_contents($path.'/'.'import-data.json'));
        foreach ($json->features as $o){
            $this->geometryObjectService->import($o);
        }

    }

    public function actionEditGeometry($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model=$this->findModel($id);
        $form = new ObjectGeometryForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $this->geometryObjectService->editGeometry($model, $form->geojson);
            return ['status'=>'success'];
            //TODO: add try catch error handler here
        }

    }

    public function actionValidateEditData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $validateForm = new ObjectDataForm();
        if(\Yii::$app->request->isAjax && $validateForm->load(\Yii::$app->request->post())) {
            return ActiveForm::validate($validateForm);
        }

    }

    public function actionEditData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $validateForm = new ObjectDataForm();
        if(\Yii::$app->request->isAjax && $validateForm->load(\Yii::$app->request->post()) && $validateForm->validate()) {
            $objectData = $this->geometryObjectService->editData($validateForm);
            return $objectData;
            //TODO: add try catch error handler here
        }
    }

    public function actionDelete($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model=$this->findModel($id);
        $this->geometryObjectService->deleteObject($model);
        return ['status'=>'success'];
        //TODO: add try catch error handler here
    }

    public function actionList()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $this->geometryObjectService->listGeojson();
    }

    protected function findModel($id)
    {
        if (($model = GeometryObject::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('advertising', 'The requested page does not exist.'));
    }
}
