<?php

return [
    'user' => [
        'class' => Da\User\Module::class,
        'administrators' =>['admin',],
        'enableFlashMessages' => false,
        'allowAccountDelete' => false,
        'emailChangeStrategy' => 'MailChangeStrategyInterface::TYPE_INSECURE',
        'enableGDPRcompliance'=>true,
        'GDPRprivacyPolicyUrl'=>'/gdpr',
        'controllerMap' => [
            'security' => [
                 'class' => 'app\controllers\SecurityController',
                 'layout' => '@app/views/layouts/user-form-guest',
            ],
            'registration' => [
                'class' => 'app\controllers\RegistrationController',
                'layout' => '@app/views/layouts/user-form-guest',
            ],
            'recovery' => [
                'class' => 'app\controllers\RecoveryController',
                'layout' => '@app/views/layouts/user-form-guest',
            ],
        ],

        // ...other configs from here: [Configuration Options](installation/configuration-options.md), e.g.
        // 'generatePasswords' => true,
        // 'switchIdentitySessionKey' => 'myown_usuario_admin_user_key',
    ],
    'calendar' => [
        'class' => 'app\modules\calendar\Module',
    ],
    'advertising' => [
            'class' => 'app\modules\advertising\Module',
    ],

];
