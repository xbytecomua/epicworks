<?php
$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$modules = require __DIR__ . '/modules.php';
$components = require  __DIR__ . '/components.php';

$config = [
    'id' => 'basic',
    'language' => 'en', // Set the language here
    //'supportedLanguages' => ['en' => 'English', 'de' => 'Deutsch' , 'ru'=>'Русский'],
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'dashboard',
    'bootstrap' => [
        [
            'class' => 'app\common\components\LanguageSelector',
            'supportedLanguages' => ['de', 'en', 'ru'],
        ],

    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => $modules,
    'components' => $components,
    'params' => $params,
    'as beforeRequest' => [
        'class' => 'yii\filters\AccessControl',
        'except' => ['user/*', 'dashboard/error', 'gdpr/*'],
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
        'generators' => [ //here
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
                ]
            ]
        ],
    ];
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
}

return $config;
