<?php

$db_local_path = __DIR__ . '/db-local.php';
$db = require(__DIR__ . '/db.php');

if(file_exists($db_local_path)) {
    $db = \yii\helpers\ArrayHelper::merge($db, require $db_local_path);
}

return [
    'request' => [
        // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        'cookieValidationKey' => 'Y5qW3ZuuDSkJ39qB_F-jXmKWEN0z-2BY',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
    ],
    'user' => [
        'identityClass' => \Da\User\Model\User::class,
        'loginUrl'=>['/user/login'],
    ],
    'errorHandler' => [
        'errorAction' => 'dashboard/error',
    ],
    'i18n' => [
        'translations' => [
            'advertising*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/modules/advertising/messages',
                'sourceLanguage' => 'en',
            ],
            'dashboard*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/views/i18/messages',
                'sourceLanguage' => 'en',
            ],
        ],
    ],
    'view' => [
        'theme' => [
            'pathMap' => [
                '@Da/User/resources/views' => '@app/views/user'
            ]
        ]
    ],
    'assetManager'=>[
        'linkAssets' => true,
        'appendTimestamp' => true,
        'class' => 'yii\web\AssetManager',
        'bundles' => [
            'yii\web\JqueryAsset' => [
                'js' => [
                    YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                ]
            ],
            'yii\bootstrap\BootstrapAsset' => [
                'css' => [
                    YII_ENV_DEV ? 'css/bootstrap.css' : 		'css/bootstrap.min.css',
                ]
            ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                'js' => [
                    YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                ]
            ],
        ]
    ],
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'db' => $db,
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
            '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
            '<module:\w+>/<controller:\w+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        ],
    ],

];