<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">


    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<!--<footer class="main-footer">-->
<!--    <div class="pull-right hidden-xs">-->
<!--        <b>Version</b> 2.0-->
<!--    </div>-->
<!--    <strong>Copyright &copy; 2018</strong> All rights-->
<!--    reserved.<a href="#" data-toggle="control-sidebar">Toggle Control Sidebar</a>-->
<!--</footer>-->

