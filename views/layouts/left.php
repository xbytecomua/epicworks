<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */
?>
<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => \Yii::t('usuario', 'Manage users'), 'icon' => 'users', 'url' => ['/user/admin'], 'visible' => Yii::$app->user->identity->isAdmin],
                    [
                        'label' =>  \Yii::t('advertising', 'werbung'),
                        'icon' => 'barcode',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Management', 'icon' => 'briefcase', 'url' => ['/advertising'],],
                            ['label' => \Yii::t('advertising', 'Map Editor'), 'icon' => 'map', 'url' => ['/advertising/map-editor'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
