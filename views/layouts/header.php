<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\widgets\Menu;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"></span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php
                    echo Nav::widget([
                        'items' => [
                            [
                                'label' => \Yii::t('dashboard', 'Language'),
                                'items' => [
                                    ['label' => 'Deutsch', 'url' => '?language=de'],
                                    ['label' => 'English', 'url' => '?language=en'],
                                    ['label' => 'Русский', 'url' => '?language=ru'],
                                ],
                            ],
                            [
                                'label' => \Yii::t('usuario', 'Profile'),
                                'url' => ['/user/settings/profile'],
                                //'linkOptions' => [...],
                            ],
                            [
                                'label' => \Yii::t('usuario', 'Logout').' ( '.Yii::$app->user->identity->username. ' )',
                                'url' => ['/user/logout'],
                                'linkOptions' => ['data-method' => 'post'],
                            ],

                        ],
                    'options' => ['class' =>'navbar-nav'],
                    ]);
                ?>
        </div>
    </nav>
</header>
